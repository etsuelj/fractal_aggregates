'''
Code for Fractal Aggregates (W.Kinzel/G.Reents, Physics by Computer)

This code is an edited version of the code presented in class to 
"construct various patterns using diffusion-limited aggregation". 
'''

from __future__ import division
import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt

__author__ = 'James Pang'
__editor__ = 'John Lawrence Euste'
__name__ = '__main__'

class Simulation(object):
    rmax = 1.
    rs = rmax + 3.
    rd = rmax + 5.
    rkill = 100.*rmax
    N = 100. # this can be changed to create different patterns
    lmax = int(np.sqrt(N)*8)
    xf = np.zeros([lmax, lmax])
    rx = 0.
    ry = 0.

    def jump(self):
        choice = np.random.randint(0,4)
        if choice == 0:
            self.rx += 1
        elif choice == 1:
            self.rx += -1
        elif choice == 2:
            self.rx += 1
        elif choice == 3:
            self.ry += -1

    def aggregate(self):
        self.xf[int(self.rx + self.lmax/2)][int(self.ry + self.lmax/2)]=1
        self.rmax = max(self.rmax, np.sqrt(self.rx**2+self.ry**2))

    def occupy(self):
        phi = np.random.rand()*2*np.pi
        self.rx = self.rs*np.sin(phi)
        self.ry = self.rs*np.cos(phi)

    def circlejump(self):
        phi = np.random.rand()*2*np.pi
        r = np.sqrt(self.rx**2 + self.ry**2)
        """you have to let it jump some distance"""
        self.rx += (r-self.rs)*np.sin(phi)
        self.ry += (r-self.rs)*np.cos(phi)

    def check(self):
        r = np.sqrt(self.rx**2 + self.ry**2)
        if r > self.rkill: # kapag lumagpas sa kill
            return 'k' # kill
        elif r >= self.rd: # kapag in between middle circle and kill circle
            return 'c' # jump then continue
        elif self.xf[int(self.rx + 1 + self.lmax//2)][int(self.ry + self.lmax//2)] + \
        self.xf[int(self.rx - 1 + self.lmax//2)][int(self.ry + self.lmax//2)] + \
        self.xf[int(self.rx + self.lmax//2)][int(self.ry + 1 + self.lmax//2)] + \
        self.xf[int(self.rx + self.lmax//2)][int(self.ry - 1 + self.lmax//2)] > 0.: # meron ba akong katabi?
            return 'a' # attach
        else:
            return 'j' # jump

    def update(self):
        self.occupy()
        self.jump()

        while 1:
            status = self.check()
            if status == 'k':
                self.occupy() # start again from the center
                self.jump()
            elif status == 'a':
                self.aggregate()
                self.rs = self.rmax + 3. # define inside circle
                self.rd = self.rmax + 5. # define middle circle
                self.rkill = 100.*self.rmax # define kill circle
                break

            elif status == 'j':
                self.jump()
            elif status == 'c':
                self.circlejump()

    def run(self):
        self.xf[int(self.lmax/2)][int(self.lmax/2)] = 1 # occupy center
        for i in xrange(1, int(self.N)):
            self.update()      
        self.M = len(self.xf==1)
        self.D = np.log(self.M)/np.log(self.rmax) # fractal dimension
        plt.matshow(sim.xf, cmap = cm.gray_r)
        plt.axis('off')
        plt.title('M=%i, \n $R_{max}$=%.3f \n fractal dimension $\\approx$ %.3f'%(self.M,self.rmax,self.D))
        plt.savefig('DLA.png',bbox_inches='tight')
        plt.show()
        
if __name__ == "__main__":
    sim = Simulation()
    sim.run()